package com.tabian.saveanddisplaysql;

public class List_data {
    private int ID;
    private  String name;
    private int cnt;

    public void setmQuantity(int mQuantity) {
        this.cnt = mQuantity;
    }


    public List_data(int ID, String name, int cnt) {
        this.name = name;
        this.cnt = cnt;
    }
    public String getName() {
        return name;
    }

    public int getCnt(){
        return cnt;
    }

    public int getId(){
        return ID;
    }

    public void addToQuantity(){
        this.cnt += 1;

    }

    public void removeFromQuantity(){
        if(this.cnt > 1){
            this.cnt -= 1;
        }
    }
}
