package com.tabian.saveanddisplaysql;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Ayo on 17/04/2017.
 */

public class OrderAdapter extends ArrayAdapter<List_data>{
    private List<List_data> list;
    private Context context;

    TextView currentName,
            quantityText,
            addMeal,
            subtractMeal,
            removeMeal;


    DatabaseHelper mDatabaseHelper;

    private String selectedName;
    private String selectedID;


    public OrderAdapter(Context context, List<List_data> myOrders) {

        super(context, 0, myOrders);
        this.list = myOrders;
        this.context = context;
    }

    public View getView(final int position, View convertView, ViewGroup parent){
        View listItemView = convertView;
        if(listItemView == null){
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.item_my_meal,parent,false
            );
        }

        final List_data current = getItem(position);

        currentName = (TextView)listItemView.findViewById(R.id.selected_name);
        subtractMeal = (TextView)listItemView.findViewById(R.id.minus_meal);
        quantityText = (TextView)listItemView.findViewById(R.id.quantity);
        addMeal = (TextView)listItemView.findViewById(R.id.plus_meal);
        removeMeal = (TextView)listItemView.findViewById(R.id.delete_item);

        mDatabaseHelper = new DatabaseHelper(this.context);

//        selectedID = current.getId(); //NOTE: -1 is just the default value




        //Set the text of the meal, amount and quantity
        currentName.setText(current.getName());
        quantityText.setText("x "+ current.getCnt());

        //OnClick listeners for all the buttons on the ListView Item
        addMeal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                current.addToQuantity();
                quantityText.setText("x "+ current.getCnt());
                notifyDataSetChanged();
                selectedName = list.get(position).getName();
                int newCnt = current.getCnt();
                mDatabaseHelper.updateQuantity(newCnt,selectedName);
            }
        });

        subtractMeal.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                current.removeFromQuantity();
                quantityText.setText("x "+current.getCnt());
                notifyDataSetChanged();
                selectedName = list.get(position).getName();
                int newCnt = current.getCnt();
                mDatabaseHelper.updateQuantity(newCnt,selectedName);
            }
        });

        removeMeal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedName = list.get(position).getName();
                list.remove(position);
                notifyDataSetChanged();
                mDatabaseHelper.delete(selectedName);
            }
        });

        return listItemView;
    }


}