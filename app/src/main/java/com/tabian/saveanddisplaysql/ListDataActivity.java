package com.tabian.saveanddisplaysql;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.WorkbookUtil;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;

/**
 * Created by User on 2/28/2017.
 */

public class ListDataActivity extends AppCompatActivity {

    private static final String TAG = "ListDataActivity";
    TextView mealTotalText;
    DatabaseHelper mDatabaseHelper;
    Button clearAll;
    Button share;
    Button back;
    Button save;

    ArrayList<List_data> orders;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_layout);

        mealTotalText = (TextView)findViewById(R.id.meal_total);
        clearAll = (Button)findViewById(R.id.clearAll);
        share = (Button)findViewById(R.id.share);
//        save = (Button)findViewById(R.id.button_save);
        back = (Button)findViewById(R.id.back_button);

        ListView storedOrders = (ListView)findViewById(R.id.listView);

        orders = getListItemData();
        OrderAdapter adapter = new OrderAdapter(this, orders);

        storedOrders.setAdapter(adapter);
        adapter.registerDataSetObserver(observer);


        clearAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDatabaseHelper.delete("");
                toastMessage("removed from database");
                Intent intent = new Intent(ListDataActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ListDataActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cursor data =  mDatabaseHelper.getData();
//
//                HashMap<String, String> hsMap = new HashMap<String, String> (  );
//                Gson gsss = new Gson ();
//
//                while (data.moveToNext ()){
//                    hsMap.put(data.getString ( 1 ), data.getString ( 2 ));
//                }
//                String retGson = gsss.toJson ( hsMap );

                XSSFWorkbook workbook = new XSSFWorkbook();
<<<<<<< HEAD
                XSSFSheet sheet = workbook.createSheet(WorkbookUtil.createSafeSheetName("sheet1"));
=======
                XSSFSheet sheet = workbook.createSheet(WorkbookUtil.createSafeSheetName("mysheet"));
>>>>>>> 0eee3ea612390faa81f23691f1a3ebf244f3c524

//                for (int i=0;i<10;i++) {
//                    Row row = sheet.createRow(i);
//                    Cell cell = row.createCell(0);
//                    cell.setCellValue(i);
//                    Cell cell1 = row.createCell(1);
//                    cell1.setCellValue(i);
//                }
                int count = data.getCount();
                int i=0;


                while (data.moveToNext ()){
                    Row row = sheet.createRow(i);
                    Cell cell = row.createCell(0);
                    cell.setCellValue(data.getString ( 1 ));
                    Cell cell1 = row.createCell(1);
                    cell1.setCellValue(data.getString ( 2 ));
                    i++;
                }
                String outFileName = "filetoshare.xlsx";
                try {
                    File cacheDir = getCacheDir();
                    File outFile = new File(cacheDir, outFileName);
                    OutputStream outputStream = new FileOutputStream(outFile.getAbsolutePath());
                    workbook.write(outputStream);
                    outputStream.flush();
                    outputStream.close();
//                    toastMessage("sdfasdfasdf");
                    shareto(outFileName, getApplicationContext());
                } catch (Exception e) {
                    /* proper exception handling to be here */
//                    toastMessage("sdfasdfasdf");
                }

            }
        });

//        share.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Cursor data =  mDatabaseHelper.getData();
//                toastMessage("share from database");
//                HashMap<String, String> hsMap = new HashMap<String, String> (  );
//                Gson gsss = new Gson ();
//
//                while (data.moveToNext ()){
//                    hsMap.put(data.getString ( 1 ), data.getString ( 2 ));
//                }
//                String retGson = gsss.toJson ( hsMap );
//                Intent sendIntent = new Intent();
//                sendIntent.setAction(Intent.ACTION_SEND);
//                sendIntent.putExtra(Intent.EXTRA_TEXT, retGson);
//                sendIntent.setType("text/plain");
//                startActivity(sendIntent);
//
//            }
//        });


    }
    public void shareto(String fileName, Context context) {
        Uri fileUri = Uri.parse("content://"+getPackageName()+"/"+fileName);
        toastMessage("sending "+fileUri.toString()+" ...");
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_STREAM, fileUri);
        shareIntent.setType("application/octet-stream");
        startActivity(Intent.createChooser(shareIntent, getResources().getText(R.string.send_to)));
    }

    public int calculateMealTotal(){
        int mealTotal = 0;
        for(List_data order : orders){
            mealTotal += order.getCnt();
        }
        return mealTotal;
    }

    DataSetObserver observer = new DataSetObserver() {
        @Override
        public void onChanged() {
            super.onChanged();
            setMealTotal();
        }
    };


    private ArrayList<List_data> getListItemData() {
        ArrayList<List_data> listViewItems = new ArrayList<List_data>();
        mDatabaseHelper = new DatabaseHelper(this);
        Cursor data = mDatabaseHelper.getData();
        while(data.moveToNext()){
            listViewItems.add(new List_data(data.getInt(0),data.getString(1),data.getInt(2)));
        }
        return listViewItems;
    }
    public void setMealTotal(){
//        mealTotalText.setText(calculateMealTotal());
    }
    /**
     * customizable toast
     * @param message
     */
    private void toastMessage(String message){
        Toast.makeText(this,message, Toast.LENGTH_SHORT).show();
    }

}
